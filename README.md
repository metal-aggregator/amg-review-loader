Sample input:
```json
{
  "band":"Goden",
  "album":"Beyond Darkness",
  "url":"http://www.angrymetalguy.com/goden-beyond-darkness-review/",
  "tags":[
    "2020",
    "3.5",
    "Abbath",
    "American Metal",
    "Beyond Darkness",
    "Black Sabbath",
    "Crowbar",
    "Death Metal",
    "death/doom metal",
    "Dio",
    "Doom Metal",
    "Goden",
    "Gorguts",
    "Hanzel und Gretyl",
    "Heaven and Hell",
    "Immortal",
    "May20",
    "Morbus Chron",
    "Necros Christos",
    "Paysage d'Hiver",
    "Review",
    "Reviews",
    "Sludge Metal",
    "Sunless",
    "Svart Records",
    "Sweven",
    "Winter"
  ]
}
```

Sample output:
```json
{}
```

Environment variables:
* TAG_SERVICE_URL - Default to http://10.0.75.1:6660
* REVIEW_SERVICE_URL - Default to http://10.0.75.1:6661
* REVIEW_PROCESSING_SERVICE_URL - Default to http://10.0.75.1:6662

The above defaults are from my local machine. See the Docker NAT ip4v entry from running `ipconfig`