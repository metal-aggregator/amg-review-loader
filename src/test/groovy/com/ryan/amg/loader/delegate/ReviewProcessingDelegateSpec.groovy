package com.ryan.amg.loader.delegate

import com.ryan.amg.loader.config.ServiceConfigurationProperties
import com.ryan.amg.loader.delegate.dto.CreateReviewProcessingRequestDTO
import com.ryan.amg.loader.delegate.dto.CreateReviewProcessingRequestDTOBuilder
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewProcessingDelegateSpec extends Specification {

    RestTemplate mockedRestTemplate = Mock()
    ServiceConfigurationProperties mockedServiceConfigurationProperties = Mock()

    ReviewProcessingDelegate reviewProcessingDelegate = new ReviewProcessingDelegate(mockedRestTemplate, mockedServiceConfigurationProperties)

    def "Invoking createReviewProcessingRecord calls the RestTemplate with the correct arguments"() {

        given:
            String inputReviewId = 'ReviewId-123'
            List<String> inputTags = new CreateReviewProcessingRequestDTOBuilder().build().getUnprocessedTags()

            CreateReviewProcessingRequestDTO expectedRequest = new CreateReviewProcessingRequestDTOBuilder().build()

            String expectedUrl = 'http://review-processing-service/api/reviews/{reviewId}/processing'
            String expectedReviewId = 'ReviewId-123'

            1 * mockedServiceConfigurationProperties.getReviewProcessingService() >> 'http://review-processing-service'

            1 * mockedRestTemplate.postForObject(expectedUrl, _ as CreateReviewProcessingRequestDTO, Void.class, expectedReviewId) >> { args ->
                assertReflectionEquals(expectedRequest, args.get(1))
            }

        when:
            reviewProcessingDelegate.createReviewProcessingRecord(inputReviewId, inputTags)

        then:
            noExceptionThrown()

    }

}
