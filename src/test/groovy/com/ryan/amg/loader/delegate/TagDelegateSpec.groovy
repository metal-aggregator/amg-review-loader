package com.ryan.amg.loader.delegate

import com.ryan.amg.loader.config.ServiceConfigurationProperties
import com.ryan.amg.loader.delegate.dto.CreateTagsRequestDTO
import com.ryan.amg.loader.delegate.dto.CreateTagsRequestDTOBuilder
import com.ryan.amg.loader.delegate.dto.CreateTagsResponseDTO
import com.ryan.amg.loader.delegate.dto.CreateTagsResponseDTOBuilder
import com.ryan.amg.loader.domain.Tag
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagDelegateSpec extends Specification {

    RestTemplate mockedRestTemplate = Mock()
    ServiceConfigurationProperties mockedServiceConfigurationProperties = Mock()

    TagDelegate tagDelegate = new TagDelegate(mockedRestTemplate, mockedServiceConfigurationProperties)

    def 'Invoking createTags correctly constructs the DTO, calls the RestTemplate, and returns the expected Tags'() {

        given:
            List<String> inputTags = ['Tag1', 'Tag2']
            String inputReviewId = 'SomeReview-123'

            List<String> expectedRestTemplateTags = ['Tag1', 'Tag2']

            CreateTagsRequestDTO expectedRequest = new CreateTagsRequestDTOBuilder().withTags(expectedRestTemplateTags).withReviewId('SomeReview-123').build()

            CreateTagsResponseDTO mockedResponse = new CreateTagsResponseDTOBuilder().build()

            List<Tag> expectedTags = new CreateTagsResponseDTOBuilder().build().getTags()

            1 * mockedServiceConfigurationProperties.getTagService() >> 'http://tag-service'
            1 * mockedRestTemplate.postForObject('http://tag-service/api/tags', _ as CreateTagsRequestDTO, CreateTagsResponseDTO.class) >> { args ->
                assertReflectionEquals(expectedRequest, args.get(1))
                return mockedResponse
            }

        when:
            List<Tag> actualTags = tagDelegate.createTags(inputTags, inputReviewId)

        then:
            assertReflectionEquals(expectedTags, actualTags)

    }

}
