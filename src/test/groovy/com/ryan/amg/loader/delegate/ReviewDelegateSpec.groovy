package com.ryan.amg.loader.delegate

import com.ryan.amg.loader.config.ServiceConfigurationProperties
import com.ryan.amg.loader.domain.AlbumReview
import com.ryan.amg.loader.domain.AlbumReviewBuilder
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewDelegateSpec extends Specification {

    RestTemplate mockRestTemplate = Mock()
    ServiceConfigurationProperties mockServiceConfigurationProperties = Mock()

    ReviewDelegate reviewDelegate = new ReviewDelegate(mockRestTemplate, mockServiceConfigurationProperties)

    @Unroll
    def "Invoking createAlbumReview calls the RestTemplate and returns the expected AlbumReview"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview expectedInputAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview mockedOutputAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview expectedOutputAlbumReview = new AlbumReviewBuilder().withCreated(expectedCreatedFlag).build()

            1 * mockServiceConfigurationProperties.getReviewService() >> 'http://mock:1234'

            1 * mockRestTemplate.postForEntity('http://mock:1234/api/reviews', _ as AlbumReview, AlbumReview.class) >> { args ->
                assertReflectionEquals(expectedInputAlbumReview, args.get(1))
                return new ResponseEntity<>(mockedOutputAlbumReview, mockedResponseStatus)
            }

        when:
            AlbumReview actualOutputAlbumReview = reviewDelegate.createAlbumReview(inputAlbumReview)

        then:
            assertReflectionEquals(expectedOutputAlbumReview, actualOutputAlbumReview)

        where:
            mockedResponseStatus | expectedCreatedFlag
            HttpStatus.CREATED   | true
            HttpStatus.OK        | false

    }

}
