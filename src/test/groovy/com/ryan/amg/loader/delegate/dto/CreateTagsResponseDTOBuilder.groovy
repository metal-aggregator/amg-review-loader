package com.ryan.amg.loader.delegate.dto

import com.ryan.amg.loader.domain.Tag
import com.ryan.amg.loader.domain.TagBuilder
import com.ryan.amg.loader.domain.TagType

class CreateTagsResponseDTOBuilder {

    List<Tag> tags = [
        new TagBuilder().withTagName('SomeTag1').withType(TagType.BAND),
        new TagBuilder().withTagName('SomeTag2').withType(TagType.UNKNOWN)
    ]

    CreateTagsResponseDTOBuilder withTags(List<Tag> tags) {
        this.tags = tags
        return this
    }

    CreateTagsResponseDTO build() {
        return new CreateTagsResponseDTO(
            tags: tags
        )
    }

}
