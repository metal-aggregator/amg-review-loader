package com.ryan.amg.loader.delegate.dto

class CreateReviewProcessingRequestDTOBuilder {

    List<String> unprocessedTags = ['Unprocessed1', 'Unprocessed2']

    CreateReviewProcessingRequestDTOBuilder withUnprocessedTags(List<String> unprocessedTags) {
        this.unprocessedTags = unprocessedTags
        return this
    }

    CreateReviewProcessingRequestDTO build() {
        return new CreateReviewProcessingRequestDTO(
            unprocessedTags: unprocessedTags
        )
    }
}
