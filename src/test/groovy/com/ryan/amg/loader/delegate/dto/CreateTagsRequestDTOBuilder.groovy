package com.ryan.amg.loader.delegate.dto

class CreateTagsRequestDTOBuilder {
    
    private List<String> tags = ['Tag1', 'Tag2']
    private String reviewId = 'reviewId-123'

    CreateTagsRequestDTOBuilder withTags(List<String> tags) {
        this.tags = tags
        return this
    }

    CreateTagsRequestDTOBuilder withReviewId(String reviewId) {
        this.reviewId = reviewId
        return this
    }

    CreateTagsRequestDTO build() {
        return new CreateTagsRequestDTO(
            tags: tags,
            reviewId: reviewId
        )
    }
}
