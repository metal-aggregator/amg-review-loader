package com.ryan.amg.loader.domain.mapper

import com.ryan.amg.loader.delegate.TagDelegate
import com.ryan.amg.loader.domain.AlbumReview
import com.ryan.amg.loader.domain.AlbumReviewBuilder
import com.ryan.amg.loader.domain.Tag
import com.ryan.amg.loader.domain.TagBuilder
import com.ryan.amg.loader.domain.TagType
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class AlbumReviewTagMapperSpec extends Specification {

    TagDelegate mockTagDelegate = Mock()

    AlbumReviewTagMapper albumReviewTagMapper = new AlbumReviewTagMapper(mockTagDelegate)

    def "Calling applyTagsToAlbumReview() correctly applies the tags to the proper AlbumReview fields by TagType"() {
        given:
            List<Tag> mockedTags = [new TagBuilder().withTagName('BandTag').withType(TagType.BAND).build(),
                                    new TagBuilder().withTagName('GenreTag').withType(TagType.GENRE).build(),
                                    new TagBuilder().withTagName('ScoreTag').withType(TagType.SCORE).build(),
                                    new TagBuilder().withTagName('OtherTag').withType(TagType.OTHER).build(),
                                    new TagBuilder().withTagName('Unknown').withType(TagType.UNKNOWN).build()]
            List<String> mockedTagNames = ['BandTag', 'GenreTag', 'ScoreTag', 'OtherTag', 'UnknownTag']
            List<String> expectedTagNames = ['BandTag', 'GenreTag', 'ScoreTag', 'OtherTag', 'UnknownTag']

            1 * mockTagDelegate.getTags(_ as List<String>) >> { args ->
                assertReflectionEquals(expectedTagNames, args.get(0))
                return mockedTags
            }

            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withTags(mockedTagNames).build()

        when:
            albumReviewTagMapper.applyTagsToAlbumReview(inputAlbumReview)

        then:
            inputAlbumReview.relatedBands == ['BandTag']
            inputAlbumReview.genres == ['GenreTag']
            inputAlbumReview.score == 'ScoreTag'
    }

}
