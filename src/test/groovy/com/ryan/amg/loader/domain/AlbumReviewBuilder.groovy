package com.ryan.amg.loader.domain

class AlbumReviewBuilder {
    String id = 'id-12345'
    String band = 'Amon Amarth'
    String album = 'The Crusher'
    String url = 'http://amonamarth.com'
    List<String> tags = ['Death Metal', 'Viking Metal', 'Melodic Death Metal']
    String imageUrl = 'http://amonamarth.com/logo.jpg'
    boolean created

    AlbumReviewBuilder withId(String id) {
        this.id = id
        return this
    }

    AlbumReviewBuilder withBand(String band) {
        this.band = band
        return this
    }

    AlbumReviewBuilder withAlbum(String album) {
        this.album = album
        return this
    }

    AlbumReviewBuilder withUrl(String url) {
        this.url = url
        return this
    }

    AlbumReviewBuilder withTags(List<String> tags) {
        this.tags = tags
        return this
    }

    AlbumReviewBuilder withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl
        return this
    }

    AlbumReviewBuilder withCreated(boolean created) {
        this.created = created
        return this
    }

    AlbumReview build() {
        return new AlbumReview(
            id: id,
            band: band,
            album: album,
            url: url,
            tags: tags,
            imageUrl: imageUrl,
            created: created
        )
    }

}
