package com.ryan.amg.loader.domain

class TagBuilder {
    String tagName = 'Grindcore'
    TagType type = TagType.GENRE

    TagBuilder withTagName(String tagName) {
        this.tagName = tagName
        return this
    }

    TagBuilder withType(TagType type) {
        this.type = type
        return this
    }

    Tag build() {
        return new Tag(
            tagName: tagName,
            type: type
        )
    }

}
