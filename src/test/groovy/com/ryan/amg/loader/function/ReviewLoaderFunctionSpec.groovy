package com.ryan.amg.loader.function

import com.ryan.amg.loader.domain.AlbumReview
import com.ryan.amg.loader.domain.AlbumReviewBuilder
import com.ryan.amg.loader.service.AlbumReviewLoaderService
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewLoaderFunctionSpec extends Specification {

    AlbumReviewLoaderService mockAlbumReviewLoaderService = Mock()

    ReviewLoaderFunction reviewLoaderFunction = new ReviewLoaderFunction(mockAlbumReviewLoaderService)

    def "Invoking apply() correctly calls the AlbumReviewLoaderService and returns 'Success'"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().build()

            1 * mockAlbumReviewLoaderService.loadReview(_ as AlbumReview) >> { args ->
                assertReflectionEquals(expectedAlbumReview, (AlbumReview) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
            }

        when:
            String actualResult = reviewLoaderFunction.apply(inputAlbumReview)

        then:
            actualResult == 'Success'

    }

}
