package com.ryan.amg.loader.service

import com.ryan.amg.loader.delegate.ReviewDelegate
import com.ryan.amg.loader.delegate.ReviewProcessingDelegate
import com.ryan.amg.loader.delegate.TagDelegate
import com.ryan.amg.loader.domain.AlbumReview
import com.ryan.amg.loader.domain.AlbumReviewBuilder
import com.ryan.amg.loader.domain.TagBuilder
import com.ryan.amg.loader.domain.TagType
import com.ryan.amg.loader.domain.mapper.AlbumReviewTagMapper
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class AlbumReviewLoaderServiceSpec extends Specification {

    ReviewDelegate mockedReviewDelegate = Mock()
    TagDelegate mockedTagDelegate = Mock()
    ReviewProcessingDelegate mockedReviewProcessingDelegate = Mock()
    AlbumReviewTagMapper mockedAlbumReviewTagMapper = Mock()

    AlbumReviewLoaderService albumReviewLoaderService = new AlbumReviewLoaderService(mockedAlbumReviewTagMapper, mockedReviewDelegate, mockedTagDelegate, mockedReviewProcessingDelegate)

    @Unroll
    def "Invoking loadReview calls the TagService to load the album's tags, filters the upserted tags when there are #description, and passes the unprocessed tags to the ReviewProcessingDelegate"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withId(null).build()
            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().withId(null).build()
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().withId('Id-123').withCreated(true).build()

            List<String> expectedTags = inputAlbumReview.getTags().collect()

            1 * mockedAlbumReviewTagMapper.applyTagsToAlbumReview(_ as AlbumReview) >> { args ->
                assertReflectionEquals(expectedAlbumReview, args.get(0))
            }

            1 * mockedReviewDelegate.createAlbumReview(_ as AlbumReview) >> { args ->
                assertReflectionEquals(expectedAlbumReview, args.get(0))
                return mockedAlbumReview
            }

            1 * mockedTagDelegate.createTags(_ as List<String>, 'Id-123') >> { args ->
                assertReflectionEquals(expectedTags, args.get(0))
                return mockedUpsertedTags
            }

            1 * mockedReviewProcessingDelegate.createReviewProcessingRecord('Id-123', _ as List<String>) >> { args ->
                assertReflectionEquals(expectedUnprocessedTags, args.get(1))
            }

        when:
            albumReviewLoaderService.loadReview(inputAlbumReview)

        then:
            noExceptionThrown()

        where:
            description             | mockedUpsertedTags                                                                                                                               | expectedUnprocessedTags
            'all unprocessed tags'  | [new TagBuilder().withTagName('Tag1').withType(TagType.UNKNOWN).build(), new TagBuilder().withTagName('Tag2').withType(TagType.UNKNOWN).build()] | ['Tag1', 'Tag2']
            'some unprocessed tags' | [new TagBuilder().withTagName('Tag1').withType(TagType.BAND).build(), new TagBuilder().withTagName('Tag2').withType(TagType.UNKNOWN).build()]    | ['Tag2']
            'no unprocessed tags'   | [new TagBuilder().withTagName('Tag1').withType(TagType.SCORE).build(), new TagBuilder().withTagName('Tag2').withType(TagType.GENRE).build()]     | []

    }

    def "Invoking loadReview calls the ReviewDelegate to load a pre-existing review, and doesn't call the Tag or Processing delegates"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withId(null).build()
            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().withId(null).build()
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().withId('Id-123').withCreated(false).build()

            1 * mockedReviewDelegate.createAlbumReview(_ as AlbumReview) >> { args ->
                assertReflectionEquals(expectedAlbumReview, args.get(0))
                return mockedAlbumReview
            }

            0 * mockedTagDelegate.createTags(_, _)
            0 * mockedReviewProcessingDelegate.createReviewProcessingRecord(_, _)

        when:
            albumReviewLoaderService.loadReview(inputAlbumReview)

        then:
            noExceptionThrown()

    }

    @Unroll
    def "Invoking loadReview() where band=#inputBand and album=#inputAlbum results in #description"() {
        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withAlbum(inputAlbum).withBand(inputBand).build()
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().withCreated(false).build()

            expectedCreateDelegateCount * mockedReviewDelegate.createAlbumReview(_ as AlbumReview) >> mockedAlbumReview

        when:
            albumReviewLoaderService.loadReview(inputAlbumReview)

        then:
            noExceptionThrown()

        where:
        inputBand   | inputAlbum  | expectedCreateDelegateCount | description
        'UNKNOWN'   | 'Something' | 1                           | 'the review being created as normal'
        'Something' | 'UNKNOWN'   | 1                           | 'the review being created as normal'
        'UNKNOWN'   | 'UNKNOWN'   | 0                           | 'the review not being created'
    }

}
