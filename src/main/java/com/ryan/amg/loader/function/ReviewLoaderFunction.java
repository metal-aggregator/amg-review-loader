package com.ryan.amg.loader.function;

import com.google.gson.Gson;
import com.ryan.amg.loader.domain.AlbumReview;
import com.ryan.amg.loader.service.AlbumReviewLoaderService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component("com.ryan.amg.loader.handler.aws.ReviewLoaderHandler")
@AllArgsConstructor
public class ReviewLoaderFunction implements Function<AlbumReview, Object> {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewLoaderFunction.class);

    private final AlbumReviewLoaderService albumReviewLoaderService;

    @Override
    public Object apply(AlbumReview albumReview) {
        LOG.debug("Function invocation input: " + new Gson().toJson(albumReview));
        albumReviewLoaderService.loadReview(albumReview);
        return "Success";
    }

}
