package com.ryan.amg.loader.exception;

public class TagNotFoundException extends RuntimeException {

    private static final String ERROR_MESSAGE_TEMPLATE = "The tag with tagName=%s was not found.";

    public TagNotFoundException(String tagName) {
        super(String.format(ERROR_MESSAGE_TEMPLATE, tagName));
    }
}
