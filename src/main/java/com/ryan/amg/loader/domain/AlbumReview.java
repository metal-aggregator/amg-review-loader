package com.ryan.amg.loader.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumReview {
    private String id;
    private String band;
    private String album;
    private String url;
    private List<String> tags = new ArrayList<>();
    private List<String> genres = new ArrayList<>();
    private List<String> relatedBands = new ArrayList<>();
    private String score;
    private String imageUrl;
    private boolean created;
}
