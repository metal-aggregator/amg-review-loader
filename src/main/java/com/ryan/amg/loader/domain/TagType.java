package com.ryan.amg.loader.domain;

public enum TagType {
    GENRE,
    BAND,
    SCORE,
    OTHER,
    UNKNOWN
}
