package com.ryan.amg.loader.domain.mapper;

import com.ryan.amg.loader.delegate.TagDelegate;
import com.ryan.amg.loader.domain.AlbumReview;
import com.ryan.amg.loader.domain.Tag;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class AlbumReviewTagMapper {

    private final TagDelegate tagDelegate;

    public void applyTagsToAlbumReview(AlbumReview targetAlbumReview) {
        List<Tag> tags = tagDelegate.getTags(targetAlbumReview.getTags());
        tags.forEach(k -> applyTagToAlbumReview(targetAlbumReview, k));
    }

    private void applyTagToAlbumReview(AlbumReview targetAlbumReview, Tag tag) {
        switch (tag.getType()) {
            case BAND:
                targetAlbumReview.getRelatedBands().add(tag.getTagName());
                break;
            case GENRE:
                targetAlbumReview.getGenres().add(tag.getTagName());
                break;
            case SCORE:
                targetAlbumReview.setScore(tag.getTagName());
                break;
            default:
                break;
        }
    }

}
