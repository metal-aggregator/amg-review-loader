package com.ryan.amg.loader.delegate;

import com.ryan.amg.loader.config.ServiceConfigurationProperties;
import com.ryan.amg.loader.delegate.dto.CreateReviewProcessingRequestDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@AllArgsConstructor
public class ReviewProcessingDelegate {

    private static final String REVIEW_PROCESSING_SERVICE_CREATE_URI = "/api/reviews/{reviewId}/processing";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public void createReviewProcessingRecord(String reviewId, List<String> unprocessedTags) {
        String targetUri = serviceConfigurationProperties.getReviewProcessingService() + REVIEW_PROCESSING_SERVICE_CREATE_URI;
        CreateReviewProcessingRequestDTO reviewProcessingRequestDTO = new CreateReviewProcessingRequestDTO(unprocessedTags);
        restTemplate.postForObject(targetUri, reviewProcessingRequestDTO, Void.class, reviewId);
    }

}
