package com.ryan.amg.loader.delegate;

import com.ryan.amg.loader.config.ServiceConfigurationProperties;
import com.ryan.amg.loader.delegate.dto.CreateTagsRequestDTO;
import com.ryan.amg.loader.delegate.dto.CreateTagsResponseDTO;
import com.ryan.amg.loader.domain.Tag;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
@AllArgsConstructor
public class TagDelegate {

    private final static String TAG_SERVICE_CREATE_URI = "/api/tags";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public List<Tag> getTags(List<String> tags) {
        LinkedMultiValueMap<String, String> queryStringParams = new LinkedMultiValueMap<>();
        queryStringParams.put("names", tags);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(serviceConfigurationProperties.getTagService() + TAG_SERVICE_CREATE_URI).queryParams(queryStringParams).build();
        return restTemplate.getForObject(uriComponents.toUriString(), CreateTagsResponseDTO.class).getTags();
    }

    public List<Tag> createTags(List<String> tags, String reviewId) {
        CreateTagsRequestDTO createTagsRequest = new CreateTagsRequestDTO(tags, reviewId);
        return restTemplate.postForObject(serviceConfigurationProperties.getTagService() + TAG_SERVICE_CREATE_URI, createTagsRequest, CreateTagsResponseDTO.class).getTags();
    }

}
