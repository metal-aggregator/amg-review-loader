package com.ryan.amg.loader.delegate;

import com.ryan.amg.loader.config.ServiceConfigurationProperties;
import com.ryan.amg.loader.domain.AlbumReview;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class ReviewDelegate {

    private final static String REVIEW_SERVICE_CREATE_URI = "/api/reviews";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public AlbumReview createAlbumReview(AlbumReview albumReview) {
        ResponseEntity<AlbumReview> responseEntity = restTemplate.postForEntity(serviceConfigurationProperties.getReviewService() + REVIEW_SERVICE_CREATE_URI, albumReview, AlbumReview.class);
        AlbumReview responseReview = responseEntity.getBody();
        responseReview.setCreated(determineIfCreated(responseEntity.getStatusCode()));
        return responseReview;
    }

    private boolean determineIfCreated(HttpStatus responseStatus) {
        return responseStatus == HttpStatus.CREATED;
    }

}
