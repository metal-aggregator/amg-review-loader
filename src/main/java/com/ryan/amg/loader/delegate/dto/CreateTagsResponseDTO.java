package com.ryan.amg.loader.delegate.dto;

import com.ryan.amg.loader.domain.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTagsResponseDTO {
    private List<Tag> tags;
}
