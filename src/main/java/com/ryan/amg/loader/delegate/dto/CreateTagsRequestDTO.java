package com.ryan.amg.loader.delegate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTagsRequestDTO {
    private List<String> tags;
    private String reviewId;
}
