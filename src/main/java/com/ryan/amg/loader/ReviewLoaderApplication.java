package com.ryan.amg.loader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewLoaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewLoaderApplication.class, args);
    }
}
