package com.ryan.amg.loader.service;

import com.google.gson.Gson;
import com.ryan.amg.loader.delegate.ReviewDelegate;
import com.ryan.amg.loader.delegate.ReviewProcessingDelegate;
import com.ryan.amg.loader.delegate.TagDelegate;
import com.ryan.amg.loader.domain.AlbumReview;
import com.ryan.amg.loader.domain.Tag;
import com.ryan.amg.loader.domain.TagType;
import com.ryan.amg.loader.domain.mapper.AlbumReviewTagMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class AlbumReviewLoaderService {

    private static final Logger LOG = LoggerFactory.getLogger(AlbumReviewLoaderService.class);
    private static final String UNKNOWN_PLACEHOLDER = "UNKNOWN";

    private final AlbumReviewTagMapper albumReviewTagMapper;
    private final ReviewDelegate reviewDelegate;
    private final TagDelegate tagDelegate;
    private final ReviewProcessingDelegate reviewProcessingDelegate;

    public void loadReview(AlbumReview inputAlbumReview) {
        if (isMalformedAlbumReview(inputAlbumReview)) {
            LOG.info("Input AlbumReview is not well formed. Skipping. Json={}", new Gson().toJson(inputAlbumReview));
            return;
        }

        albumReviewTagMapper.applyTagsToAlbumReview(inputAlbumReview);
        AlbumReview albumReview = reviewDelegate.createAlbumReview(inputAlbumReview);

        if (albumReview.isCreated()) {
            List<Tag> upsertedTags = tagDelegate.createTags(albumReview.getTags(), albumReview.getId());
            List<String> unprocessedTagNames = determineUnprocessedTagNames(upsertedTags);
            reviewProcessingDelegate.createReviewProcessingRecord(albumReview.getId(), unprocessedTagNames);
        }
    }

    private boolean isMalformedAlbumReview(AlbumReview albumReview) {
        return UNKNOWN_PLACEHOLDER.equalsIgnoreCase(albumReview.getBand()) && UNKNOWN_PLACEHOLDER.equalsIgnoreCase(albumReview.getAlbum());
    }

    private List<String> determineUnprocessedTagNames(List<Tag> allTagsForReview) {
        return allTagsForReview.stream().filter(k -> k.getType() == TagType.UNKNOWN).map(Tag::getTagName).collect(Collectors.toList());
    }

}
