package com.ryan.amg.loader.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "metal.services")
public class ServiceConfigurationProperties {
    private String tagService;
    private String reviewService;
    private String reviewProcessingService;
}
